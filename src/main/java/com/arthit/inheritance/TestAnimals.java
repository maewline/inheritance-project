/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arthit.inheritance;

/**
 *
 * @author Arthit
 */
public class TestAnimals {

    public static void main(String[] args) {
        Animal animal = new Animal("Ani", 4, "white");
        Dog dang = new Dog("Dang", "Black&White");
        Dog mome = new Dog("Mome", "WhiteBody&BlackFace");
        Dog to = new Dog("To", "Brown");
        Dog bat = new Dog("Bat", "WhiteBody&BlackFace");
        Cat zero = new Cat("Zero", "Orange");
        Duck zom = new Duck("Zom", "Orange");
        Duck gapgap = new Duck("GapGap", "Black");
        
        animal.speak();
        animal.walk();

        dang.speak();
        dang.walk();

        zero.speak();
        zero.walk();
      
        zom.speak();
        zom.walk();
        zom.Fly();
        
        TextLoopShow();
        Animal[] animals = {dang, zero, zom, mome, to, bat, gapgap, animal};
        for (int i = 0; i < animals.length; i++) {
            System.out.println("Number: "+(i+1));
            if (animals[i] instanceof Duck) {
                ShowBehavior(animals, i);
                ((Duck)animals[i]).Fly(); 
                 System.out.println(animals[i].name+" is Animal: "+(animals[i] instanceof Animal));
                 System.out.println(animals[i].name+" is Duck: "+(animals[i] instanceof Duck));
                 System.out.println(animals[i].name+" is Object: "+(animals[i] instanceof Object));
                 Separator();
            }else{
                if (animals[i] instanceof Dog) {
                 ShowBehavior(animals, i);
                 System.out.println(animals[i].name+" is Animal: "+(animals[i] instanceof Animal));
                 System.out.println(animals[i].name+" is Dog: "+(animals[i] instanceof Dog));
                 System.out.println(animals[i].name+" is Object: "+(animals[i] instanceof Object));
                 Separator();
                }
               else if (animals[i] instanceof Cat) {
                 ShowBehavior(animals, i);
                 System.out.println(animals[i].name+" is Animal: "+(animals[i] instanceof Animal));
                 System.out.println(animals[i].name+" is Cat: "+(animals[i] instanceof Cat));
                 System.out.println(animals[i].name+" is Object: "+(animals[i] instanceof Object));
                 Separator();
                }
               else if (animals[i] instanceof Animal) {
                 ShowBehavior(animals, i);
                 System.out.println(animals[i].name+" is Animal: "+(animals[i] instanceof Animal));
                 System.out.println(animals[i].name+" is Dog: "+(animals[i] instanceof Dog));
                 System.out.println(animals[i].name+" is Cat: "+(animals[i] instanceof Cat));
                 System.out.println(animals[i].name+" is Duck: "+(animals[i] instanceof Duck));
                 System.out.println(animals[i].name+" is Object: "+(animals[i] instanceof Object));
                 Separator();
                }
           }
        }
    }

    private static void TextLoopShow() {
        System.out.println("");
        System.out.println("Turn Of Loop!!");
    }

    private static void ShowBehavior(Animal[] animals, int i) {
        animals[i].speak();
        animals[i].walk();
    }

    public static void Separator() {
        System.out.println("..................................");
    }

}
